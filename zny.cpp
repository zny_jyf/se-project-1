#include <string>
#include <cstring>
#include <cmath>
#include <iostream>

using std::cout;
using std::string;

int toInt32(string &str) {
    int sum = 0;
    for (int i = 0; i < str.length(); ++i)
        sum = sum * 10 + str[i] - '0';
    return sum;
}

float toDouble(string &str) {
    int pos = str.find(".");
    string str_integer = str.substr(0, pos);
    string str_fraction;
    if (pos == string::npos || pos == str.length() - 1)
        str_fraction = string("0");
    else
        str_fraction = str.substr(pos + 1, str.length());
    double ret = toInt32(str_integer)
                + toInt32(str_fraction) / pow(10, str_fraction.length());
    return ret;
}

int main() {
    string a("123");
    cout << toDouble(a);
    return 0;
}