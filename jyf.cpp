#include <iostream>
#include <string>
#include "math.h"

bool str_to_int32(std::string s, int32_t & output)
{
    int len = s.length(), temp;
    output = 0;
    bool result = true;
    for(int i = len - 1; i >= 0; i--)
    {
        if(s[i] >= '0' && s[i] <= '9'){
            output += (s[i] - '0') * temp;
        }
        else{
            result = false;
            break;
        }
        temp = temp * 10;
    }
    return result;
}

bool str_to_double(std::string s, double & output)
{
    int len = s.length(), mark = 0;
    int32_t a, b;
    output = 0;
    bool  result = false;
    for(int i = len - 1; i >= 0; i--){
        if(s[i] == '.'){
            mark = i;
            break;
        }
    }
    std::string first = s.substr(0, mark);
    std::string second = s.substr(mark+1, len);
    if(str_to_int32(first, a) == true && str_to_int32(second, b) == true){
        output = a + b / pow(10, (len - mark - 1));
        result = true;
    }
    return result;
}
